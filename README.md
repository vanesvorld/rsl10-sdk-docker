# ONSemiconductor RSL10 CI ready container

## Usage
Example [RSL10 Eddystone beacon](https://gitlab.com/arturmadrzak/rsl10-beacon) project.
### Continous Integration
### Local
```
docker pull ``
docker run --rm -it -v "$(pwd)":/workdir -w=/workdir registry.gitlab.com/arturmadrzak/rsl10-sdk-docker make
```
## Disclaimer 
This repository is not conntected in any way with ONSemiconductor company. This is my private initiative to simplify CI and development processed in projects based on RSL10 SoC. 
